<?php
/**
 * Created by PhpStorm.
 * User: ami
 * Date: 9/21/17
 * Time: 5:52 PM
 */

namespace App\Repository;


use App\Entity\Offer;
use DataLayer\DataTransformer\DataTransformerDefinition as Definition;
use DataLayer\DataTransformer\TypeTransformer;

class OfferRepository extends AppRepository
{

    protected function getTableName()
    {
        return 'offer';
    }

    protected function createEntityInstance()
    {
        return new Offer();
    }

    protected function getColMap()
    {
        return [
            'id' => 'id',
            'name' => 'name',
            'is_enabled' => 'enabled',
            'created_time' => 'createdTime',
            'updated_time' => 'updatedTime',
            'installs_limit_per_day' => 'installsLimitPerDay',
            'content' => 'content',
        ];
    }

    protected function getTransformMap()
    {
        return [
            'created_time' => new Definition(TypeTransformer::TYPE_DATETIME),

            'updated_time' => new Definition(TypeTransformer::TYPE_DATETIME, [
                'nullable' => true,
            ]),
        ];
    }

    public function filter(array $filterData = [])
    {
        $params = [];
        $whereClause = '1 = 1';

        if(isset($filterData['country']) && $this->isPosInt($filterData['country'])) {
            $whereClause .= ' and (po.country_id = :country) ';
            $params['country'] = $this->myToInt($filterData['country']);
        }
        if(isset($filterData['package']) && $this->isPosInt($filterData['package'])) {
            $whereClause .= ' and (po.package_id = :package) ';
            $params['package'] = $this->myToInt($filterData['package']);
        }
        if(isset($filterData['name']) && 0 < mb_strlen(trim($filterData['name']))) {
            $whereClause .= ' and (ofr.name like :name) ';
            $params['name'] = '%' . trim($filterData['name']) . '%';
        }


        $query = sprintf(
            'select ofr.* from offer ofr left outer join package_offer po on (ofr.id = po.offer_id) where %s',
            $whereClause
        );

        $reply = $this->dao->getResult($query, $params);
        $res = [];
        foreach ($reply as $row) {
            $res []= $this->getDataTransformer()->rowToEntity($row, $this->createEntityInstance());
        }
        return $res;
    }

    private function isPosInt($n)
    {
        return is_numeric($n) && 0 < intval($n, 10);
    }

    private function myToInt($n)
    {
        if(!is_numeric($n)) {
            $n = 0;
        }
        $n = intval($n, 10);
        if(!(0 < $n)) {
            $n = 0;
        }
        return $n;
    }

    public function findByPackage($packageId)
    {
        $query = 'select o.* from package_offer po inner join offer o on (po.offer_id = o.id) where package_id = :packageId order by po.`order` asc';


        $reply = $this->dao->getResult($query, ['packageId' => $packageId]);
        $res = [];
        foreach ($reply as $row) {
            $res []= $this->getDataTransformer()->rowToEntity($row, $this->createEntityInstance());
        }
        return $res;
    }
}