<?php
/**
 * Created by PhpStorm.
 * User: ami
 * Date: 9/21/17
 * Time: 11:02 AM
 */

namespace App\Repository;


use DataLayer\DAO\DAOInterface;
use DataLayer\Repository\NotFoundException;

class RepositoryFactory
{
    /** @var  DAOInterface */
    protected $dao;

    /**
     * RepositoryFactory constructor.
     * @param DAOInterface $dao
     */
    public function __construct(DAOInterface $dao)
    {
        $this->dao = $dao;
    }


    public function getRepository($name)
    {
        switch ($name) {

            case 'country':
                return new CountryRepository($this->dao);

            case 'offer':
                return new OfferRepository($this->dao);

            case 'package':
                return new PackageRepository($this->dao);

            default:
                throw new NotFoundException(sprintf('repository not found [%s]', $name));
        }
    }
}