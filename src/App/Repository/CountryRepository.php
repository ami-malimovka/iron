<?php
/**
 * Created by PhpStorm.
 * User: ami
 * Date: 9/21/17
 * Time: 11:29 AM
 */

namespace App\Repository;


use App\Entity\Country;


class CountryRepository extends AppRepository
{

    protected function createEntityInstance()
    {
        return new Country();
    }

    protected function getTableName()
    {
        return 'country';
    }

    protected function getColMap()
    {
       return [
            'id' => 'id',
            'country_code' => 'code',
            'name' => 'name',
            'is_enabled' => 'enabled',
        ];
    }

    public function findByCode($countryCode)
    {
        $query = sprintf(
            'select * from %s where country_code = :code',
            $this->getTableName()
        );

        $res = $this->dao->getSingleResult($query, ['code' => $countryCode]);
        return $this->getDataTransformer()->rowToEntity($res, $this->createEntityInstance());
    }
}