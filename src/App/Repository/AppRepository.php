<?php
/**
 * Created by PhpStorm.
 * User: ami
 * Date: 9/26/17
 * Time: 11:20 PM
 */

namespace App\Repository;

use DataLayer\Repository\Repository;

abstract class AppRepository extends Repository
{
    public function findEnabled()
    {
        $query = sprintf(
            'select * from %s where is_enabled = 1',
            $this->getTableName()
        );

        $reply = $this->dao->getResult($query);
        $res = [];
        foreach ($reply as $row) {
            $res []= $this->getDataTransformer()->rowToEntity($row, $this->createEntityInstance());
        }
        return $res;
    }
}