<?php
/**
 * Created by PhpStorm.
 * User: ami
 * Date: 9/21/17
 * Time: 5:52 PM
 */

namespace App\Repository;


use App\Entity\Package;
use DataLayer\DataTransformer\DataTransformerDefinition as Definition;
use DataLayer\DataTransformer\TypeTransformer;


class PackageRepository extends AppRepository
{

    protected function getTableName()
    {
        return 'package';
    }

    protected function createEntityInstance()
    {
        return new Package();
    }

    protected function getColMap()
    {
        return [
            'id' => 'id',
            'name' => 'name',
            'created_time' => 'createdTime',
            'updated_time' => 'updatedTime',
            'is_enabled' => 'enabled',
        ];
    }

    protected function getTransformMap()
    {
        return [
            'created_time' => new Definition(TypeTransformer::TYPE_DATETIME),

            'updated_time' => new Definition(TypeTransformer::TYPE_DATETIME, [
                'nullable' => true,
            ]),
        ];
    }
}