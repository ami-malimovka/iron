<?php
/**
 * Created by PhpStorm.
 * User: ami
 * Date: 9/21/17
 * Time: 10:01 AM
 */

namespace App\Entity;


class Offer
{
    private $id;

    private $name;

    private $createdTime;

    private $updatedTime;

    private $enabled;

    private $installsLimitPerDay;

    private $content;

    /**
     * Offer constructor.
     */
    public function __construct()
    {
        $dtNow = new \DateTime('now');
        $this
            ->setName('')
            ->setCreatedTime($dtNow)
            ->setUpdatedTime($dtNow)
            ->setInstallsLimitPerDay(0)
            ->setContent('')
            ->setEnabled(false)
        ;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCreatedTime()
    {
        return $this->createdTime;
    }

    /**
     * @param mixed $createdTime
     */
    public function setCreatedTime($createdTime)
    {
        $this->createdTime = $createdTime;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getUpdatedTime()
    {
        return $this->updatedTime;
    }

    /**
     * @param mixed $updatedTime
     */
    public function setUpdatedTime($updatedTime)
    {
        $this->updatedTime = $updatedTime;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEnabled()
    {
        return $this->enabled;
    }

    /**
     * @param mixed $enabled
     */
    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getInstallsLimitPerDay()
    {
        return $this->installsLimitPerDay;
    }

    /**
     * @param mixed $installsLimitPerDay
     */
    public function setInstallsLimitPerDay($installsLimitPerDay)
    {
        $this->installsLimitPerDay = $installsLimitPerDay;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param mixed $content
     */
    public function setContent($content)
    {
        $this->content = $content;
        return $this;
    }


}