<?php
/**
 * Created by PhpStorm.
 * User: ami
 * Date: 9/21/17
 * Time: 10:06 AM
 */

namespace App\Entity;


class Country
{
    private $id;

    private $name;

    private $enabled;

    private $code;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }


    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getEnabled()
    {
        return $this->enabled;
    }


    public function setEnabled($enabled)
    {
        $this->enabled = $enabled;
        return $this;
    }


    public function getCode()
    {
        return $this->code;
    }


    public function setCode($code)
    {
        $this->code = $code;
        return $this;
    }


}