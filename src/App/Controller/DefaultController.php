<?php
/**
 * Created by PhpStorm.
 * User: ami
 * Date: 9/23/17
 * Time: 4:09 PM
 */

namespace App\Controller;


class DefaultController extends Controller
{
    public function homeAction()
    {
        return $this->renderResponse('Default/home.html.twig');
    }
}