<?php
/**
 * Created by PhpStorm.
 * User: ami
 * Date: 9/25/17
 * Time: 11:21 PM
 */

namespace App\Controller;


class MappingController extends Controller
{
    const MAX_OFFERS = 10;

    public function queryAction()
    {
        return $this->renderResponse('Mapping/query.html.twig', [
            'countries' => $this->get('repository')->getRepository('country')->findEnabled(),
            'maxOffersPerPackage' => self::MAX_OFFERS,
        ]);
    }
}