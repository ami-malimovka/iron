<?php

/**
 * Created by PhpStorm.
 * User: ami
 * Date: 9/17/17
 * Time: 4:41 PM
 */
namespace App\Controller;

use App\Entity\Country;
use DataLayer\Repository\Repository;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class TestController extends Controller
{

    public function testAction(Request $request)
    {
        /** @var Repository $repo */
        $repo = $this->get('repository')->getRepository('country');

        $newCountry = new Country();
        $newCountry
            ->setCode('JJJ')
            ->setName('JIFIWAWA')
            ->setEnabled(1)
        ;
        $repo->persist($newCountry);

        $newCountry->setName('KAKI');
        $repo->persist($newCountry);

        $data = $repo->findAll();

        return $this->renderResponse('Test/testPage.html.twig', [
            'countries' => $data,
        ]);
    }

    public function byeAction(Request $request)
    {
        return new Response($this->render('Test/sample.html.twig', ['cake' => 'is nice']));
    }
}