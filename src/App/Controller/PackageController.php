<?php
/**
 * Created by PhpStorm.
 * User: ami
 * Date: 9/21/17
 * Time: 5:58 PM
 */

namespace App\Controller;


use App\Entity\Package;
use DataLayer\Repository\Repository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

class PackageController extends Controller
{
    public function indexAction()
    {
        /** @var Repository $repo */
        $repo = $this->get('repository')->getRepository('package');

        return $this->renderResponse('Package/index.html.twig', [
            'entities' => $repo->findAll(),
        ]);
    }

    public function editAction($id)
    {
        $entity = $this->get('repository')->getRepository('package')->find($id);

        if(null === $entity) {
            throw new ResourceNotFoundException('failed to find Package entity');
        }

        return $this->renderResponse('Package/edit.html.twig', [
            'entity' => $entity,
            'returnRoute' => $this->generateRoute('package_update', [
                'id' => $id,
            ]),
        ]);
    }

    public function updateAction($id, Request $request)
    {
        /** @var Package $entity */
        $entity = $this->get('repository')->getRepository('package')->find($id);

        if(null === $entity) {
            throw new ResourceNotFoundException('failed to find Package entity');
        }

        $formData = $request->get('theForm', []);
        if(0 < count($formData)) {
            if(!isset($formData['enabled'])) {
                $formData['enabled'] = 0;
            }

            $this->applyFormDataToEntity($formData, $entity);

            $entity->setUpdatedTime(new \DateTime('now'));
            $this->get('repository')->getRepository('package')->persist($entity);

            $this->notifyActionSuccessfull();
            return $this->redirect('package_edit', [
                'id' => $id,
            ]);
        }

        $this->notifyActionFailed();
        return $this->renderResponse('Package/edit.html.twig', [
            'entity' => $entity,
        ]);
    }

    public function newAction(Request $request)
    {
        return $this->renderResponse('Package/new.html.twig');
    }

    public function createAction(Request $request)
    {
        $formData = $request->get('theForm', []);
        if(0 < count($formData)) {
            $entity  = new Package();
            $dtNow = new \DateTime('now');

            $entity
                ->setCreatedTime($dtNow)
                ->setUpdatedTime($dtNow)
            ;

            if(!isset($formData['enabled'])) {
                $formData['enabled'] = 0;
            }

            $this->applyFormDataToEntity($formData, $entity);



            $this->get('repository')->getRepository('package')->persist($entity);
            $this->notifyActionSuccessfull();
            return $this->redirect('package_edit', [
                'id' => $entity->getId(),
            ]);
        }

        $this->notifyActionFailed();
        return $this->renderResponse('Package/new.html.twig');
    }

    public function deleteAction($id)
    {
        /** @var Package $entity */
        $entity = $this->get('repository')->getRepository('package')->find($id);

        if(null === $entity) {
            throw new ResourceNotFoundException('failed to find Package entity');
        }

        $this->get('repository')->getRepository('package')->remove($entity);
        $this->notifyActionSuccessfull();

        return $this->redirect('package');
    }

}