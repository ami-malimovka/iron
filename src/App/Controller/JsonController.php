<?php
/**
 * Created by PhpStorm.
 * User: ami
 * Date: 9/25/17
 * Time: 10:00 PM
 */

namespace App\Controller;


use App\Entity\Country;
use App\Entity\Offer;
use App\Entity\Package;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class JsonController extends Controller
{
    public function selectAction(Request $request)
    {
        $formName = 'json_create_form';
        if(0 < count($request->get($formName, []))) {
            $formData = $request->get($formName);

            /** @var Country $country */
            $country = $this->get('repository')->getRepository('country')->find($formData['country']);
            /** @var Package $package */
            $package = $this->get('repository')->getRepository('package')->find($formData['package']);

            if(!$country) {
                $this->notifyActionFailed('Bad country chosen');
            } elseif (!$package) {
                $this->notifyActionFailed('Bad package chosen');
            } else {
                return $this->redirect('json_create', [
                    'packageId' => $package->getId(),
                    'countryCode' => $country->getCode(),
                ]);
            }
        }

        return $this->renderResponse('Json/select.html.twig', [
            'formName' => $formName,
            'countries' => $this->get('repository')->getRepository('country')->findEnabled(),
            'packages' => $this->get('repository')->getRepository('package')->findEnabled(),
        ]);
    }

    public function createAction($packageId, $countryCode)
    {
        /** @var Package $package */
        $package = $this->get('repository')->getRepository('package')->find($packageId);

        /** @var Country $country */
        $country = $this->get('repository')->getRepository('country')->findByCode($countryCode);

        /** @var Offer[] $offers */
        $offers = $this->get('repository')->getRepository('offer')->findByPackage($packageId);

        return new JsonResponse($this->get('package_json_builder')->buildJson(
            $package,
            $country,
            $offers
        ));
    }
}