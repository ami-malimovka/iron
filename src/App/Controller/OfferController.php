<?php
/**
 * Created by PhpStorm.
 * User: ami
 * Date: 9/21/17
 * Time: 5:58 PM
 */

namespace App\Controller;


use App\Entity\Offer;
use App\Repository\OfferRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;

class OfferController extends Controller
{
    public function indexAction(Request $request)
    {
        /** @var OfferRepository $repo */
        $repo = $this->get('repository')->getRepository('offer');

        $filterFormData = $request->get('filter_form', []);
        if(0 === count($filterFormData)) {
            $entities = $repo->findAll();
        } else {
            $entities = $repo->filter($filterFormData);
        }

        return $this->renderResponse('Offer/index.html.twig', [
            'entities' => $entities,
            'countries' => $this->get('repository')->getRepository('country')->findAll(),
            'packages' => $this->get('repository')->getRepository('package')->findAll(),
            'filterFormData' => $filterFormData,
        ]);
    }

    public function editAction($id)
    {
        $entity = $this->get('repository')->getRepository('offer')->find($id);

        if(null === $entity) {
            throw new ResourceNotFoundException('failed to find Offer entity');
        }

        return $this->renderResponse('Offer/edit.html.twig', [
            'entity' => $entity,
            'returnRoute' => $this->generateRoute('offer_update', [
                'id' => $id,
            ]),
        ]);
    }

    public function updateAction($id, Request $request)
    {
        /** @var Offer $entity */
        $entity = $this->get('repository')->getRepository('offer')->find($id);

        if(null === $entity) {
            throw new ResourceNotFoundException('failed to find Offer entity');
        }

        $formData = $request->get('theForm', []);
        if(0 < count($formData)) {
            if(!isset($formData['enabled'])) {
                $formData['enabled'] = 0;
            }

            $this->applyFormDataToEntity($formData, $entity);

            $entity->setUpdatedTime(new \DateTime('now'));
            $this->get('repository')->getRepository('offer')->persist($entity);

            $this->notifyActionSuccessfull();
            return $this->redirect('offer_edit', [
                'id' => $id,
            ]);
        }

        $this->notifyActionFailed();
        return $this->renderResponse('Offer/edit.html.twig', [
            'entity' => $entity,
        ]);
    }

    public function newAction(Request $request)
    {
        return $this->renderResponse('Offer/new.html.twig');
    }

    public function createAction(Request $request)
    {
        $formData = $request->get('theForm', []);
        if(0 < count($formData)) {
            $entity  = new Offer();
            $dtNow = new \DateTime('now');

            $entity
                ->setCreatedTime($dtNow)
                ->setUpdatedTime($dtNow)
            ;

            if(!isset($formData['enabled'])) {
                $formData['enabled'] = 0;
            }

            $this->applyFormDataToEntity($formData, $entity);



            $this->get('repository')->getRepository('offer')->persist($entity);
            $this->notifyActionSuccessfull();
            return $this->redirect('offer_edit', [
                'id' => $entity->getId(),
            ]);
        }

        $this->notifyActionFailed();
        return $this->renderResponse('Offer/new.html.twig');
    }

    public function deleteAction($id)
    {
        /** @var Offer $entity */
        $entity = $this->get('repository')->getRepository('offer')->find($id);

        if(null === $entity) {
            throw new ResourceNotFoundException('failed to find Offer entity');
        }

        $this->get('repository')->getRepository('offer')->remove($entity);
        $this->notifyActionSuccessfull();

        return $this->redirect('offer');
    }

}