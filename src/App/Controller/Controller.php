<?php
/**
 * Created by PhpStorm.
 * User: ami
 * Date: 9/17/17
 * Time: 11:06 PM
 */

namespace App\Controller;

use Simplex\ContainerAware\ContainerAware;
use Simplex\ContainerAware\ServiceNotFoundException;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class Controller extends ContainerAware
{
    protected function renderResponse($template, array $params = [])
    {
        $content = $this->render($template, $params);

        return new Response($content);
    }

    protected function render($template, array $params = [])
    {
        $defaultParams = [

            /* site base url - for referencing web assets */
            'HOME' => preg_replace(
                '/[\\/]?index.php[\\/]?/',
                '',
                $this->generateRoute('HOME', [], UrlGeneratorInterface::ABSOLUTE_URL)
            ),

        ];

        return $this->get('twig')->render($template, array_merge($params, $defaultParams));
    }

    protected function generateRoute(
        $route,
        array $params = [],
        $type = UrlGeneratorInterface::ABSOLUTE_PATH
    ) {
        return $this->get('route_generator')->generate(
            $route,
            $params,
            $type
        );
    }

    protected function get($id)
    {
        if (!$this->container->has($id)) {
            throw new ServiceNotFoundException(sprintf('failed to load service [%s]', $id));
        }

        return $this->container->get($id);
    }

    protected function redirect(
        $route,
        array $params = []
    ) {
        return new RedirectResponse($this->generateRoute($route, $params));
    }

    protected function applyFormDataToEntity(array $formData, $entity)
    {
        $accessor = PropertyAccess::createPropertyAccessor();
        foreach ($formData as $formFieldName => $formFieldValue) {
            if ($accessor->isWritable($entity, $formFieldName)) {
                $accessor->setValue($entity, $formFieldName, $formFieldValue);
            }
        }
    }

    protected function notifyActionSuccessfull($msg = 'action was successful')
    {
        $this->addFlashMsg($msg, 'success_flash_msg');
    }

    protected function addFlashMsg($msg, $type='flash_msg')
    {
        $this->get('session')->getFlashBag()->add(
            $type, $msg
        );
    }

    protected function notifyActionFailed($msg = 'action failed')
    {
        $this->addFlashMsg($msg, 'fail_flash_msg');
    }
}