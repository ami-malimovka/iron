<?php
/**
 * Created by PhpStorm.
 * User: ami
 * Date: 9/25/17
 * Time: 10:49 PM
 */

namespace App\PackageJsonBuilder;


use App\Entity\Country;
use App\Entity\Offer;
use App\Entity\Package;

class PackageJsonBuilder
{
    const DATE_FORMAT = 'Y-m-d H:i:s';

    /** @param Package $package
     * @param Country $country
     * @param Offer[] $offers
     */
    public function buildJson(
        $package,
        $country,
        $offers
    )
    {
        $res = [
            'name' => $package->getName(),
            'is_enabled' => $package->getEnabled(),
            'id' => $package->getId(),
            'country' => $country->getCode(),
            'file_created_at' => (new \DateTime('now'))->format(self::DATE_FORMAT),
            'offers' => []
        ];

        foreach ($offers as $offer) {
            $res['offers'] []= [
                'name' => $offer->getName(),
                'is_enabled' => $offer->getEnabled(),
                'content' => $offer->getContent(),
                'created_time' => $offer->getCreatedTime()->format(self::DATE_FORMAT),
                'installs_limit_per_day' => $offer->getInstallsLimitPerDay(),
                'id' => $offer->getId(),
            ];
        }

        return $res;
    }
}