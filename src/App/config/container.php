<?php
/**
 * Created by PhpStorm.
 * User: ami
 * Date: 9/17/17
 * Time: 10:14 PM
 */
use Pimple\Container;
use Pimple\Psr11\Container as PsrContainer;

use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpKernel\Controller\ArgumentResolver;
use Simplex\Controller\ControllerResolver;
use Symfony\Component\Routing;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\Session\Session;

// TODO: this is cluttered. the definitions need to be in a simpler human friendly text form, and be compiled by a builder
$container = new Container();

/****************************/
/***  container params  *****/
/****************************/

/** database connection params */
$container['dao.db'] = 'iron';
$container['dao.user'] = 'iron';
$container['dao.pass'] = '123';
$container['dao.host'] = '127.0.0.1';
$container['dao.charset'] = 'utf8';

/** twig params */
$container['twig.twig_templates_path'] = __DIR__.'/../views';
$container['twig.twig_cache_path'] = __DIR__.'/../../../cache/twig';
/******************************/



$container['twig'] = function ($c) {
    $loader = new Twig_Loader_Filesystem($c['twig.twig_templates_path']);
    $twig = new Twig_Environment($loader, [
        /*'cache' => $c['twig.twig_cache_path'],*/
    ]);

    //TODO: the whole twig extending needs to be somewhere else, this is messy
    $function = new Twig_SimpleFunction('path', function (
        $route,
        array $params = [],
        $type = UrlGeneratorInterface::ABSOLUTE_PATH
    )  use($c) {
        return $c['route_generator']->generate(
            $route,
            $params,
            $type
        );
    });
    $twig->addFunction($function);

    $twig->addGlobal('session', $c['session']);

    return $twig;
};

$container['session'] = function($c) {
    return new Session();
};

$container['event_dispatcher'] = function($c) {
    return new EventDispatcher();
};

$container['controller_resolver'] = function($c) {
    return new ControllerResolver(new PsrContainer($c));
};

$container['argument_resolver'] = function ($c) {
    return new ArgumentResolver();
};

$container['context'] = function ($c) {
    return new Routing\RequestContext();
};

$container['routes'] = $routes;

$container['matcher'] = function ($c) {
    return new Routing\Matcher\UrlMatcher($c['routes'], $c['context']);;
};

$container['route_generator'] = function ($c) {
    return new Routing\Generator\UrlGenerator($c['routes'], $c['context']);
};

$container['app_kernel'] = function ($c) {
    $subscribers = [];
    if(isset($c['event_subscribers'])) {
        $subscribers = $c['event_subscribers'];
    }

    return new Simplex\AppKernel(
        $c['matcher'],
        $c['controller_resolver'],
        $c['argument_resolver'],
        $c['event_dispatcher'],
        $subscribers
    );
};

$container['event_subscribers'] = [
    new \App\EventSubscriber\TestListener(),
];


/** data layer */

// good chance this needs to be singleton
$container['dao'] = function ($c) {
    return new \DataLayer\DAO\MySQLDAO(
        $c['dao.db'],
        $c['dao.user'],
        $c['dao.pass']
    );
};

$container['repository'] = function ($c) {
    return new \App\Repository\RepositoryFactory(
        $c['dao']
    );
};

$container['package_json_builder']= function ($c) {
    return new App\PackageJsonBuilder\PackageJsonBuilder();
};


/** end of data layer */


return new PsrContainer($container);