<?php
use Symfony\Component\Routing;

//TODO: it would be nice to split the routing across several files, and have something build the routes collection from that...

$routes = new Routing\RouteCollection();

$routes->add('HOME', new Routing\Route('/', [
    '_controller' => 'App\Controller\DefaultController::homeAction',
]));


$routes->add('bye', new Routing\Route('/bye', [
    '_controller' => 'App\Controller\TestController::byeAction',
]));

$routes->add('test', new Routing\Route('/test-page', [
    '_controller' => 'App\Controller\TestController::testAction',
]));


/************/
/*** offer **/
/************/
$routes->add('offer', new Routing\Route('/offer', [
    '_controller' => 'App\Controller\OfferController::indexAction',
]));

$routes->add('offer_edit', new Routing\Route('/offer/{id}/edit', [
    '_controller' => 'App\Controller\OfferController::editAction',
]));

$routes->add('offer_update', new Routing\Route('/offer/{id}/update', [
    '_controller' => 'App\Controller\OfferController::updateAction',
]));

$routes->add('offer_new', new Routing\Route('/offer/new', [
    '_controller' => 'App\Controller\OfferController::newAction',
]));

$routes->add('offer_create', new Routing\Route('/offer/create', [
    '_controller' => 'App\Controller\OfferController::createAction',
]));

$routes->add('offer_delete', new Routing\Route('/offer/{id}/delete', [
    '_controller' => 'App\Controller\OfferController::deleteAction',
]));
/*** end of offer **/



/***************/
/*** package **/
/***************/
$routes->add('package', new Routing\Route('/package', [
    '_controller' => 'App\Controller\PackageController::indexAction',
]));

$routes->add('package_edit', new Routing\Route('/package/{id}/edit', [
    '_controller' => 'App\Controller\PackageController::editAction',
]));

$routes->add('package_update', new Routing\Route('/package/{id}/update', [
    '_controller' => 'App\Controller\PackageController::updateAction',
]));

$routes->add('package_new', new Routing\Route('/package/new', [
    '_controller' => 'App\Controller\PackageController::newAction',
]));

$routes->add('package_create', new Routing\Route('/package/create', [
    '_controller' => 'App\Controller\PackageController::createAction',
]));

$routes->add('package_delete', new Routing\Route('/package/{id}/delete', [
    '_controller' => 'App\Controller\PackageController::deleteAction',
]));
/*** end of package **/


/*************************/
/*******  Json  **********/
/*************************/

$routes->add('json_select', new Routing\Route('/json/select', [
    '_controller' => 'App\Controller\JsonController::selectAction',
]));

$routes->add('json_create', new Routing\Route('/json/create/{packageId}-{countryCode}', [
    '_controller' => 'App\Controller\JsonController::createAction',
]));

/*************************/


/********************************/
/*******  mapping query  ********/
/********************************/

$routes->add('mapping_query', new Routing\Route('/mapping/query', [
    '_controller' => 'App\Controller\MappingController::queryAction',
]));

/********************************/

return $routes;