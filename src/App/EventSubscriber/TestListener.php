<?php
/**
 * Created by PhpStorm.
 * User: ami
 * Date: 9/18/17
 * Time: 5:29 PM
 */

namespace App\EventSubscriber;


use App\Event\ResponseEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;


class TestListener implements EventSubscriberInterface
{

    public static function getSubscribedEvents()
    {
        return ['response' => 'onResponse'];
    }

    public function onResponse(ResponseEvent $event)
    {

        /*$response = $event->getResponse();

        $response->setContent($response->getContent().'GA COzxcDE');*/
    }
}