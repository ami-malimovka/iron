<?php
/**
 * Created by PhpStorm.
 * User: ami
 * Date: 9/17/17
 * Time: 5:19 PM
 */

namespace Simplex;


use App\Event\ResponseEvent;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Controller\ArgumentResolverInterface;
use Symfony\Component\HttpKernel\Controller\ControllerResolverInterface;
use Symfony\Component\Routing\Exception\ResourceNotFoundException;
use Symfony\Component\Routing\Matcher\UrlMatcherInterface;

class AppKernel
{
    /** @var UrlMatcherInterface */
    private $matcher;

    /** @var  ControllerResolverInterface */
    private $controllerResolver;

    /** @var  ArgumentResolverInterface */
    private $argumentResolver;

    /** @var  EventDispatcherInterface */
    private $eventDispatcher;

    /** @var  array */
    private $eventSubscribers;

    /**
     * Framework constructor.
     * @param UrlMatcherInterface $matcher
     * @param ControllerResolverInterface $controllerResolver
     * @param ArgumentResolverInterface $argumentResolver
     * @param EventDispatcherInterface $eventDispatcher
     * @param array $eventSubscribers
     */
    public function __construct(
        UrlMatcherInterface $matcher,
        ControllerResolverInterface $controllerResolver,
        ArgumentResolverInterface $argumentResolver,
        EventDispatcherInterface $eventDispatcher,
        array $eventSubscribers = []
    ) {
        $this->matcher = $matcher;
        $this->controllerResolver = $controllerResolver;
        $this->argumentResolver = $argumentResolver;
        $this->eventDispatcher = $eventDispatcher;
        $this->eventSubscribers = $eventSubscribers;
    }

    public function handle(Request $request)
    {
        $this->attachSubscribers();
        $this->matcher->getContext()->fromRequest($request);

        try {
            $request->attributes->add($this->matcher->match($request->getPathInfo()));

            $controller = $this->controllerResolver->getController($request);
            $arguments = $this->argumentResolver->getArguments($request, $controller);

            $response = call_user_func_array($controller, $arguments);

            //TODO: it would be nicer to have an Error controller, and let it create the Response from the Exception
            } catch (ResourceNotFoundException $e) {
                return new Response('Not Found', 404);
            } catch (\Exception $e) {
                return new Response('An error occurred', 500);
            }

        $this->eventDispatcher->dispatch('response', new ResponseEvent($response, $request));

        return $response;
    }

    private function attachSubscribers()
    {
        foreach ($this->eventSubscribers as $subscriber) {
            $this->eventDispatcher->addSubscriber($subscriber);
        }
    }
}