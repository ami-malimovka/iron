<?php
/**
 * Created by PhpStorm.
 * User: ami
 * Date: 9/17/17
 * Time: 10:20 PM
 */

namespace Simplex\Controller;

use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;
use Simplex\ContainerAware\ContainerAwareInterface;
use Symfony\Component\HttpKernel\Controller\ControllerResolver as BaseControllerResolver;

class ControllerResolver extends BaseControllerResolver
{
    private $container;

    public function __construct(
        ContainerInterface $container,
        LoggerInterface $logger = null
    )
    {
        $this->container = $container;

        parent::__construct($logger);
    }


    /**
     * Returns an instantiated controller.
     *
     * @param string $class A class name
     *
     * @return object
     */
    protected function instantiateController($class)
    {
        $controller = parent::instantiateController($class);

        if ($controller instanceof ContainerAwareInterface) {
            $controller->setContainer($this->container);
        }

        return $controller;
    }
}