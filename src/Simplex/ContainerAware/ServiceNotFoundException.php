<?php
/**
 * Created by PhpStorm.
 * User: ami
 * Date: 9/18/17
 * Time: 8:54 AM
 */

namespace Simplex\ContainerAware;


use Psr\Container\NotFoundExceptionInterface;
use Simplex\SimplexException;

class ServiceNotFoundException
    extends SimplexException
    implements NotFoundExceptionInterface
{

}