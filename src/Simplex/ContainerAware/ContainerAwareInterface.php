<?php
/**
 * Created by PhpStorm.
 * User: ami
 * Date: 9/17/17
 * Time: 10:26 PM
 */

namespace Simplex\ContainerAware;


use Psr\Container\ContainerInterface;

interface ContainerAwareInterface
{
    public function setContainer(ContainerInterface $container = null);
}