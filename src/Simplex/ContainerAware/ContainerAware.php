<?php
/**
 * Created by PhpStorm.
 * User: ami
 * Date: 9/17/17
 * Time: 11:07 PM
 */

namespace Simplex\ContainerAware;


use Psr\Container\ContainerInterface;

class ContainerAware implements ContainerAwareInterface
{
    /** @var  ContainerInterface */
    protected $container;

    /**
     * @param mixed $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }


}