<?php
/**
 * Created by PhpStorm.
 * User: ami
 * Date: 9/22/17
 * Time: 8:36 PM
 */

require_once __DIR__.'/../../vendor/autoload.php';
require_once __DIR__.'/../../vendor/twig/twig/lib/Twig/Autoloader.php';
Twig_Autoloader::register();