<?php
/**
 * Created by PhpStorm.
 * User: ami
 * Date: 9/19/17
 * Time: 5:23 PM
 */

namespace DataLayer\DAO;


class MySQLDAO implements DAOInterface
{
    private $host;
    private $db;
    private $user;
    private $pass;
    private $charset;

    private $pdo;

    /**
     * MySQLDAO constructor.
     * @param string $db
     * @param string $user
     * @param string $pass
     * @param string $host
     * @param string $charset
     */
    public function __construct(
        $db,
        $user,
        $pass,
        $host = '127.0.0.1',
        $charset = 'utf8'
    )
    {
        $this->host = $host;
        $this->db = $db;
        $this->user = $user;
        $this->pass = $pass;
        $this->charset = $charset;
    }

    public function execQuery($query, array $params = [])
    {
        return '00000' === $this->executeStatement($query, $params)->errorCode();
    }

    /** @return \PDOStatement */
    private function executeStatement($query, array $params = [])
    {
        $stmt = $this->getPDO()->prepare($query);
        $stmt->execute($params);
        return $stmt;
    }

    /**
     * @return \PDO
     */
    public function getPDO()
    {
        if(null === $this->pdo) {
            $this->pdo = new \PDO(
                $this->getDSN(),
                $this->user,
                $this->pass,
                $this->getOptArray()
            );
        }
        return $this->pdo;
    }

    private function getDSN()
    {
        return sprintf(
            'mysql:host=%s;dbname=%s;charset=%s',
            $this->host,
            $this->db,
            $this->charset
        );
    }

    private function getOptArray()
    {
        return [
            \PDO::ATTR_ERRMODE            => \PDO::ERRMODE_EXCEPTION,
            \PDO::ATTR_DEFAULT_FETCH_MODE => \PDO::FETCH_ASSOC,
            \PDO::ATTR_EMULATE_PREPARES   => false,
        ];
    }

    public function getSingleResult($query, array $params = [])
    {
        return $this->executeStatement($query, $params)->fetch();
    }

    public function getResult($query, array $params = [])
    {
        return $this->executeStatement($query, $params)->fetchAll();
    }

    public function insert($query, array $params = [])
    {
        $res = $this->executeStatement($query, $params);

        if(false === $res) {
            return false;
        }
        return $this->getPDO()->lastInsertId();
    }

}