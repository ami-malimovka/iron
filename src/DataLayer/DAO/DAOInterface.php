<?php
/**
 * Created by PhpStorm.
 * User: ami
 * Date: 9/19/17
 * Time: 5:12 PM
 */

namespace DataLayer\DAO;

/** a Data Access Object is an object that allows us to interact with a data source */

interface DAOInterface
{
    public function execQuery($query, array $params = []);
    public function getSingleResult($query, array $params = []);
    public function getResult($query, array $params = []);
    public function insert($query, array $params = []);
}