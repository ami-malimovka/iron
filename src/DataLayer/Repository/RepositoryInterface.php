<?php
/**
 * Created by PhpStorm.
 * User: ami
 * Date: 9/19/17
 * Time: 5:14 PM
 */

namespace DataLayer\Repository;

/** a Repository mediates between the "business logic" domain and the "data source" domain */

interface RepositoryInterface
{
    /** find an entity by id */
    public function find($id);

    /** find all the entities */
    public function findAll();

    /** save an entity to the data source */
    public function persist($entity);

    /** remove an entity from the data source */
    public function remove($entity);


}