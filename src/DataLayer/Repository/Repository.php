<?php
/**
 * Created by PhpStorm.
 * User: ami
 * Date: 9/19/17
 * Time: 5:22 PM
 */

namespace DataLayer\Repository;


use DataLayer\DAO\DAOInterface;

use DataLayer\DataTransformer\DataTransformer;
use Symfony\Component\PropertyAccess\PropertyAccess;


abstract class Repository implements RepositoryInterface
{
    const TRANSFORM = 'trans';
    const REVERSE_TRANSFORM = 'rev_trans';

    /** @var  DAOInterface */
    protected $dao;
    /** @var  DataTransformer */
    protected $dataTransformer;

    /**
     * Repository constructor.
     * @param DAOInterface $dao
     */
    public function __construct(DAOInterface $dao)
    {
        $this->dao = $dao;
    }

    public function find($id)
    {
        $query = sprintf(
            'select * from %s where %s = :id',
            $this->getTableName(),
            $this->getKeyColName()
        );

        $res = $this->dao->getSingleResult($query, ['id' => $id]);
        return $this->getDataTransformer()->rowToEntity($res, $this->createEntityInstance());
    }

    abstract protected function getTableName();

    protected function getKeyColName()
    {
        return 'id';
    }

    /**
     * @return DataTransformer
     */
    protected function getDataTransformer()
    {
        if(null === $this->dataTransformer) {
            $this->dataTransformer = new DataTransformer(
                $this->getKeyColName(),
                $this->getColMap(),
                $this->getTransformMap()
            );
        }
        return $this->dataTransformer;
    }

    abstract protected function getColMap();

    protected function getTransformMap()
    {
        return [];
    }

    abstract protected function createEntityInstance();

    public function findAll()
    {
        $query = sprintf(
            'select * from %s',
            $this->getTableName()
        );

        $reply = $this->dao->getResult($query);
        $res = [];
        foreach ($reply as $row) {
            $res []= $this->getDataTransformer()->rowToEntity($row, $this->createEntityInstance());
        }
        return $res;
    }

    public function persist($entity)
    {
        if(null === $this->getKeyValue($entity)) {
            return $this->createNewEntity($entity);
        }
        return $this->updateEntity($entity);
    }

    protected function getKeyValue($entity) {
        $accessor = PropertyAccess::createPropertyAccessor();
        $keyFieldName = $this->mapColToField($this->getKeyColName());
        if($accessor->isReadable($entity, $keyFieldName)) {
            return $accessor->getValue($entity, $keyFieldName);
        }

        throw new RepositoryException('failed to read key from entity');
    }

    protected function mapColToField($name)
    {
        return $this->getColMap()[$name];
    }

    protected function createNewEntity($entity)
    {
        $cols = array_keys($this->getColMap());
        unset($cols[array_search($this->getKeyColName(), $cols)]);

        $query = sprintf(
            'insert into %s(%s) values(%s)',
            $this->getTableName(),
            '`' . implode('`,`', $cols) . '`',
            ':' . implode(', :', $cols)
        );

        $params = $this->getDataTransformer()->entityToRow($entity);
        $params[$this->getKeyColName()] = 0;
        unset($params[$this->getKeyColName()]);


        $res = $this->dao->insert($query, $params);
        if(false === $res) {
            return $res;
        }

        $this->setKeyValue($entity, $res);
        return true;
    }

    protected function setKeyValue($entity, $value) {
        $accessor = PropertyAccess::createPropertyAccessor();
        $keyFieldName = $this->mapColToField($this->getKeyColName());
        if($accessor->isWritable($entity, $keyFieldName)) {
            $accessor->setValue($entity, $keyFieldName, $value);
            return $entity;
        }

        throw new RepositoryException('failed to write entity key field');
    }

    protected function updateEntity($entity)
    {
        $query = sprintf(
            'update %s set %s where %s = :table_key_%s',
            $this->getTableName(),
            $this->buildUpdateStmtSetPart(),
            $this->getKeyColName(),
            $this->getTableName()
        );

        $params = $this->getDataTransformer()->entityToRow($entity);
        if(isset($params[$this->getKeyColName()])) {
            unset($params[$this->getKeyColName()]);
        }
        $params['table_key_' . $this->getTableName()] = $this->getKeyValue($entity);

        return $this->dao->execQuery($query, $params);
    }

    protected function buildUpdateStmtSetPart()
    {
        $cols = array_keys($this->getColMap());
        unset($cols[array_search($this->getKeyColName(), $cols)]);
        $res = [];
        foreach ($cols as $col) {
            $res []= sprintf('%s = :%s', $col, $col);
        }

        return implode(', ', $res);
    }

    public function remove($entity)
    {
        if(null === $entity) {
            return true;
        }
        $query = sprintf(
            'delete from %s where %s = :id',
            $this->getTableName(),
            $this->getKeyColName()
        );

        return $this->dao->execQuery($query, [
            'id' => $this->getKeyValue($entity),
        ]);
    }
}