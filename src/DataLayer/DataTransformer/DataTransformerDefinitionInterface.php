<?php
/**
 * Created by PhpStorm.
 * User: ami
 * Date: 9/23/17
 * Time: 12:48 PM
 */

namespace DataLayer\DataTransformer;

interface DataTransformerDefinitionInterface
{
    /**
     * @return string
     */
    public function getType();

    /**
     * @return array
     */
    public function getParams();
}