<?php
/**
 * Created by PhpStorm.
 * User: ami
 * Date: 9/23/17
 * Time: 12:14 PM
 */

namespace DataLayer\DataTransformer;

/** Transforms DB datum to BL datum, and back  */

interface TypeTransformer
{
    const TYPE_IDENTITY = 'identity';
    const TYPE_DATETIME = 'datetime';

    public function transform($val);

    public function reverseTransform($val);

    public function setParams(array $params = []);
}