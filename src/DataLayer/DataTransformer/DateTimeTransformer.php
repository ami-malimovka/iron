<?php
/**
 * Created by PhpStorm.
 * User: ami
 * Date: 9/23/17
 * Time: 12:19 PM
 */

namespace DataLayer\DataTransformer;


class DateTimeTransformer extends AbstractTransformer
{
    const DEFAULT_FORMET = 'Y-m-d H:i:s';

    public function transform($val)
    {
        if(null === $val && $this->isNullable()) {
            return null;
        }
        $format = $this->getFormat();
        $res = \DateTime::createFromFormat($format, $val);
        if(false === $res) {
            throw new DataTransformerException('failed to convert datum to DateTime object');
        }

        return $res;
    }

    public function reverseTransform($val)
    {
        if(null === $val && $this->isNullable()) {
            return null;
        }
        if(!($val instanceof \DateTime)) {
            throw new DataTransformerException(sprintf(
                'expected DateTime instance, got [%s]',
                is_object($val) ? get_class($val) : gettype($val)
                )
            );
        }

        return $val->format($this->getFormat());
    }

    /**
     * @return string
     */
    private function getFormat()
    {
        $format = isset($this->params['format'])
            ? $this->params['format']
            : self::DEFAULT_FORMET;

        return $format;
    }
}