<?php
/**
 * Created by PhpStorm.
 * User: ami
 * Date: 9/22/17
 * Time: 2:17 PM
 */

namespace DataLayer\DataTransformer;


use Symfony\Component\PropertyAccess\PropertyAccess;

/** utility class to hold the logic of transforming BL entities to DB rows and back */
class DataTransformer
{
    const TRANSFORM = 'trans';
    const REVERSE_TRANSFORM = 'rev_trans';

    private $keyColName;
    private $colMap;
    private $transformMap;

    /**
     * DataTransformer constructor.
     * @param $keyColName
     * @param $colMap
     * @param $transformMap
     */
    public function __construct($keyColName, array $colMap, array $transformMap)
    {
        $this->keyColName = $keyColName;
        $this->colMap = $colMap;
        $this->transformMap = $transformMap;
    }


    public function rowToEntity($dbRow, $entity) {
        if(false === $dbRow) {
            return null;
        }

        $accessor = PropertyAccess::createPropertyAccessor();
        foreach ($dbRow as $colName => $colValue) {
            $fieldValue = $this->transformColToField($colName, $colValue);
            $fieldName = $this->mapColToField($colName);
            if($accessor->isWritable($entity, $fieldName)) {
                $accessor->setValue($entity, $fieldName, $fieldValue);
            }
        }

        return $entity;
    }

    protected function transformColToField($colName, $colValue)
    {
        return $this->getTypeTransformer($colName)->transform($colValue);
    }

    /** @return TypeTransformer */
    private function getTypeTransformer($colName)
    {
        if(
            !isset($this->transformMap[$colName]) ||
            !($this->transformMap[$colName] instanceof DataTransformerDefinitionInterface)
        ) {
            return new IdentityTransformer();
        }

        /** @var DataTransformerDefinitionInterface $definition */
        $definition = $this->transformMap[$colName];

        $transformer = new IdentityTransformer();
        switch($definition->getType()) {

            case TypeTransformer::TYPE_DATETIME:
                $transformer = new DateTimeTransformer();

        }
        $transformer->setParams($definition->getParams());
        return $transformer;
    }

    protected function mapColToField($name)
    {
        return $this->colMap[$name];
    }

    public function entityToRow($entity)
    {
        $res = [];
        $accessor = PropertyAccess::createPropertyAccessor();
        $cols = array_keys($this->colMap);
        foreach ($cols as $col) {
            $fieldName = $this->mapColToField($col);
            if($accessor->isReadable($entity, $fieldName)) {
                $res[$col] = $this->transformFieldToCol($entity, $col);
            }
        }

        return $res;
    }

    protected function transformFieldToCol($entity, $colName)
    {
        $fieldName = $this->mapColToField($colName);
        $accessor = PropertyAccess::createPropertyAccessor();
        if(!$accessor->isReadable($entity, $fieldName)) {
            return null;
        }
        $fieldVal = $accessor->getValue($entity, $fieldName);

        return $this->getTypeTransformer($colName)->reverseTransform($fieldVal);
    }
}