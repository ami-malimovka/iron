<?php
/**
 * Created by PhpStorm.
 * User: ami
 * Date: 9/23/17
 * Time: 1:03 PM
 */

namespace DataLayer\DataTransformer;


abstract class AbstractTransformer implements TypeTransformer
{
    /** @var  array */
    protected $params;

    /**
     * AbstractTransformer constructor.
     */
    public function __construct()
    {
        $this->params = [];
    }

    public function setParams(array $params = [])
    {
        $this->params = $params;
    }

    protected function isNullable()
    {
        if(isset($this->params['nullable'])) {
            return (bool)$this->params['nullable'];
        }
        return false;
    }


}