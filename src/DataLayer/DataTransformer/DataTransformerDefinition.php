<?php
/**
 * Created by PhpStorm.
 * User: ami
 * Date: 9/23/17
 * Time: 12:46 PM
 */

namespace DataLayer\DataTransformer;


class DataTransformerDefinition implements DataTransformerDefinitionInterface
{
    /** @var  string */
    private $type;

    /** @var  array */
    private $params;

    /**
     * DataTransformerDefinition constructor.
     * @param string $type
     * @param array $params
     */
    public function __construct($type, array $params = [])
    {
        $this->type = $type;
        $this->params = $params;
    }

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @return array
     */
    public function getParams()
    {
        return $this->params;
    }

}