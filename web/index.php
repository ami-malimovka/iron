<?php
/**
 * Created by PhpStorm.
 * User: ami
 * Date: 9/17/17
 * Time: 4:00 PM
 */
error_reporting(E_ALL);
ini_set('display_errors', 1);

use Symfony\Component\HttpFoundation\Request;

require_once __DIR__ . '/../src/Simplex/autoloader.php';

$request = Request::createFromGlobals();

$routes = include __DIR__.'/../src/App/config/routes.php';
$container = include __DIR__.'/../src/App/config/container.php';

$response = $container->get('app_kernel')->handle($request);
$response->send();