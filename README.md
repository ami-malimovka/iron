# README #

### What is this repository for? ###

Demo app for Ironsource

### How do I get set up? ###

#### Requirements ####

 - php >= 5.6 (was not tested on php7)
 - git
 
#### Installation ####

 - clone this repository to your disk (`git clone https://bitbucket.org/ami-malimovka/iron.git`)
 - Install [composer](https://getcomposer.org/download/) and run `php composer.phar install`
 - Fill the DB connection params at `src/App/config/container.php`
 - Expose the `web` directory to the internet
 
 

### Who do I talk to? ###

Ami Malimovka
[ami.malimovka@gmail.com](mailto:ami.malimovka@gmail.com)
