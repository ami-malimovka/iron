create OR REPLACE view latest_daily_aggregate
as
select
	c.id country_id, di.country_code, di.offer_name, max(di.`date`) maxDate
from 
	daily_installs di
	inner join `country` c on (c.country_code = di.country_code and c.is_enabled = 1)
where
	di.total_views > 0
group by
	c.id, di.country_code, di.offer_name;


create OR REPLACE view latest_daily_installs
as

select
	di.`date`, T.country_id, di.country_code, di.offer_name, di.total_views, di.total_installs
from
	daily_installs di
inner join
latest_daily_aggregate T on (T.country_code = di.country_code and T.offer_name = di.offer_name and T.maxDate = di.`date`);
	