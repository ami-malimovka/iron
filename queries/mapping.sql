set @country = '';
set @num  = 0;

delete from package_offer;

insert into package_offer(`order`, offer_id, country_id, package_id)
select row_num `order`, offer_id, country_id, package_id
from (
	select T.*, @num := if(T.country_code = @country, @num + 1, 1) as row_num, @country := T.country_code as country, p.id package_id 

	from (

		(select
			di.*, o.id offer_id, di.total_installs/di.total_views
		from 
			latest_daily_installs di
			inner join offer o on (di.offer_name = o.name and o.is_enabled = 1)
		where
			di.total_views > 0
			and di.country_code = 'AD'
		order by
			di.total_installs/di.total_views desc
		limit 10) 

		union all

		(select
			di.*, o.id offer_id, di.total_installs/di.total_views
		from 
			latest_daily_installs di
			inner join offer o on (di.offer_name = o.name and o.is_enabled = 1)
		where
			di.total_views > 0
			and di.country_code = 'BM'
		order by
			di.total_installs/di.total_views desc
		limit 10) 

	) T, package p

	where
		p.is_enabled = 1
) S